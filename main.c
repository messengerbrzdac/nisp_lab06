#include <stdio.h>
#define N 4

int main()
{
    /*
    student-a - Michał Groński
    student-b - Patryk Gruszowski
    student-c - Szczepan Wójcik
    */

    int tab[N] = {1, 2, 4, 8};

    int isEvenResult = isEven(2);
    int maxResult = max(tab, N);

    printf("Result isEven = %d \n", isEvenResult);
    printf("Result max = %d \n", maxResult);

    return 0;
}
