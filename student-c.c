int isPrime(int digit) {
	int temp = 0;
	int i;
	for (i = 2; i <= digit / 2; i++) {
		if (digit % i == 0) {
			temp++;
			break;
		}
	}
	if (temp == 0 && digit != 1) {
		return 1;
	}
	return 0;
}

int average(int [] digits, int size) {
	int i;
	int sum = 0;
	for (i=0;i<size;i++) {
		sum=sum+digits[i];
	}
	return sum/size;
}
