#include "student-a.h"

int isEven(int digit){
    return digit % 2;
}

int max(int digits[], int size){
    int max = digits[0];
    for(int i = 0; i < size; i++){
        if(max < digits[i]) max = digits[i];
    }
    return max;
}
